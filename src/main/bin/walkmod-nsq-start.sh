#!/bin/sh
NSQ_DEBUG_OPTS="-Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=1044"
nohup java -cp ../../test/resources:../../../target/walkmod-nsq-1.0-jar-with-dependencies.jar org.walkmod.nsq.WalkmodNSQDispatcher  &
