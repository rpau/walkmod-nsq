nohup nsqlookupd -broadcast-address="localhost" >~/logs/nsqlookupd.log &
nohup nsqd -lookupd-tcp-address=127.0.0.1:4160 -broadcast-address="localhost" > ~/logs/nsqd.log &
nohup nsqadmin --lookupd-http-address=localhost:4161 > ~/logs/nsqadmin.log &