package org.walkmod.nsq;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.walkmod.nsq.handlers.WalkmodWorkHandler;
import org.walkmod.nsq.lookupd.BasicLookupd;
import org.walkmod.nsq.syncresponse.SyncResponseHandler;
import org.walkmod.nsq.syncresponse.SyncResponseReader;
import org.walkmod.nsq.workers.ApplyWorker;

public class WalkmodNSQThread implements Runnable {

	private String nsqlookupHost = "localhost";
	
	private String nsqdHost = "localhost";

	private int nsqlookupPort = 4161;
	
	private int nsqdPort = 4151;
	
	private String jarFile = "target/walkmod-nsq-1.0-jar-with-dependencies.jar";
	
	private String cfgDir = "src/test/resources";
	
	public static Logger LOG = Logger.getLogger(WalkmodNSQThread.class);

	private static final String PROPERTIES_FILE_NAME = "nsq.properties";

	
	public WalkmodNSQThread() throws Exception {

		Properties prop = new Properties();

		InputStream is = getClass().getClassLoader().getResourceAsStream(
				PROPERTIES_FILE_NAME);

		try {
			prop.load(is);

		} finally {
			is.close();
		}

		nsqlookupHost = prop.getProperty("nsq.nsqlookup.host");
		nsqdHost = prop.getProperty("nsq.nsqd.host");
		nsqlookupPort = Integer.parseInt(prop.getProperty("nsq.nsqlookup.port"));
		nsqdPort = Integer.parseInt(prop.getProperty("nsq.server.nsqd.port"));
		jarFile = prop.getProperty("nsq.walkmod.jar");
		cfgDir = prop.getProperty("nsq.walkmod.cfg.dir");
		
	}

	public void run() {

		SyncResponseHandler sh;
		try {
			sh = new WalkmodWorkHandler(nsqdHost, nsqdPort, jarFile, ApplyWorker.class, cfgDir);
		} catch (Exception e) {
			RuntimeException re = new RuntimeException("Error creating the walkmod NSQ [\"apply\"] handler");
			re.setStackTrace(e.getStackTrace());
			LOG.error("Error creating the walkmod NSQ handler", e);
			throw re;
		}
		SyncResponseReader reader = new SyncResponseReader("apply", "walkmod",
				sh);
		LOG.debug(nsqlookupHost + ":" + nsqlookupPort);

		reader.addLookupd(new BasicLookupd(nsqlookupHost + ":" + nsqlookupPort));
		
	}

}
