package org.walkmod.nsq;

public class WalkmodNSQDispatcher {

	public static void main(String[] args) throws Exception {
	   String androidHome = System.getenv("ANDROID_HOME");
	   if(androidHome == null){
	      System.out.println("ERROR: ANDROID_HOME is undefined!");
	   }
	   else{
	      System.out.println("ANDROID_HOME:" +androidHome);
	   }
		WalkmodNSQThread processor = new WalkmodNSQThread();
		Thread t = new Thread(processor);
		t.setDaemon(false);
		t.start();
	}
}
