package org.walkmod.nsq.handlers;

import java.io.File;
import java.io.FileNotFoundException;

import org.walkmod.nsq.Message;
import org.walkmod.nsq.exceptions.NSQException;
import org.walkmod.nsq.syncresponse.SyncResponseHandler;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class WalkmodWorkHandler implements SyncResponseHandler {

   private String host;
   private int port = 4151;

   private String jarFile = "";

   private Class<?> workerClass;

   protected Logger LOG;

   private String cfgDir;

   public WalkmodWorkHandler(String host, int port, String jarFile, Class<?> workerClass, String cfgDir) {
      this.host = host;
      this.port = port;
      this.jarFile = jarFile;
      this.workerClass = workerClass;
      this.cfgDir = cfgDir;
   }

   public void execWalkmod(String projectDir, String jarFile, Class<?> worker, String... args) throws Exception {
      File jar = new File(jarFile);

      if (!jar.exists()) {
         throw new FileNotFoundException("the nsq jar file " + jar.getAbsolutePath() + " does not exists");
      }
      String[] debug = new String[] { /*
                                       * "-Xdebug", "-Xnoagent"+ "-Djava.compiler=NONE",
                                       * "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=1045"
                                       */ };
      int size = 4 + debug.length;
      if (args != null) {
         size += args.length;
      }
      String[] cmd = new String[size];
      int j = 0;
      cmd[j++] = "java";

      for (int i = 0; i < debug.length; i++) {
         cmd[j++] = debug[i];
      }

      cmd[j++] = "-cp";
      cmd[j++] = jar.getAbsolutePath();
      cmd[j++] = worker.getName();

      if (args != null) {
         for (int i = 0; i < args.length; i++) {
            cmd[j + i] = args[i];
         }
      }

      ProcessBuilder pb = new ProcessBuilder(cmd);

      pb.directory(new File(projectDir));

      Process p = pb.start();

      /*
       * InputStream is = p.getInputStream();
       * 
       * StringWriter writer = new StringWriter(); IOUtils.copy(is, writer, "UTF-8"); String
       * theString = writer.toString();
       * 
       * p.waitFor();
       * 
       * LOG.debug(theString); is.close();
       */

   }

   @Override
   public boolean handleMessage(Message msg){
      try {
         LOG = Logger.getLogger(this.getClass());
         String message = new String(msg.getBody());
         LOG.debug("NSQ message handled. Content: " + message);
         ObjectMapper mapper = new ObjectMapper();
         mapper.configure(Feature.QUOTE_FIELD_NAMES, false);
         JsonNode json = null;

         json = mapper.readTree(message);

         String projectDir = json.get("project").asText();

         String hostArg = host + ":" + port;

         execWalkmod(projectDir, jarFile, workerClass, hostArg, cfgDir, message);
      } catch (Throwable e) {
         LOG.error("Error launching the walkmod process for  " + new String(msg.getBody()), e);
         return true;
      }

      return true;
   }

}