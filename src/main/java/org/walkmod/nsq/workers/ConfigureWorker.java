package org.walkmod.nsq.workers;

import groovy.text.GStringTemplateEngine;
import groovy.text.Template;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.walkmod.nsq.NSQProducer;
import org.walkmod.nsq.exceptions.NSQException;

import org.apache.commons.io.FileUtils;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.walkmod.conf.entities.ReaderConfig;

public class ConfigureWorker extends AbstractWalkmodWorker {
	private GStringTemplateEngine engine = null;
	private Template templateName;
	private String address;
	private String projectId;
	private List<String> folders = new LinkedList<String>();
	private String formatter = "google";
	private ArrayList<String> modifiedFiles;
	private String cfgDir;
	private JsonNode config;
	private boolean initializedProject = false;
	private File projectFormatter = null;

	public ConfigureWorker(String address, String projectId, String cfgDir,
			String config) throws Exception {
		engine = new GStringTemplateEngine();
		this.address = address;
		this.projectId = projectId;
		modifiedFiles = new ArrayList<String>();
		this.cfgDir = cfgDir;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(Feature.QUOTE_FIELD_NAMES, false);
		this.config = null;
		try {
			this.config = mapper.readTree(config);
		} catch (Exception e) {
			NSQException ex = new NSQException(
					"Error reading the parameters of " + config);
			ex.setStackTrace(e.getStackTrace());
			LOG.error("Error reading the parameters of ", e);
			throw ex;
		}
	}

	@Override
	public void preWork() throws Exception {
		File pom = new File("pom.xml");
		if (pom.exists()) {
			addMavenFolders(new File("."), folders, "src/main/java",
					"src/test/java");
			if (this.config != null && this.config.has("formatter")) {
				this.formatter = this.config.get("formatter").asText();
			}
			LOG.debug("Applying the [" + formatter + "] formatter");
			String[] includes = null;
			if (config.has("includes")) {
				JsonNode includesNode = config.get("includes");
				includes = new String[includesNode.size()];
				Iterator<JsonNode> it = includesNode.iterator();
				int i = 0;
				while (it.hasNext()) {
					includes[i] = it.next().asText();
					i++;
				}
			}
			boolean force = true;
			if (config.has("force")) {
				force = config.get("force").asBoolean();
			}
			initializedProject = createWalkmodConfig(folders, formatter,
					includes, force);
		} else {
			LOG.debug("It is not a maven project");
		}
	}

	@Override
	public void postWork() throws Exception {
		
		if (initializedProject) {
			File walkmodCfg = new File("walkmod.xml");
			walkmodCfg.delete();
			if (projectFormatter != null && projectFormatter.exists()) {
				projectFormatter.delete();
			}
		}
		List<File> files = super.getModifiedFiles();
		if (files != null) {
			for (File file : files) {
				modifiedFiles.add(file.getPath());
			}
		}

		ObjectMapper om = new ObjectMapper();
		NSQProducer producer = new NSQProducer(address, projectId);
		WorkerResponse response = new WorkerResponse();
		response.setFiles(modifiedFiles);
		producer.put(om.writeValueAsString(response));
		LOG.debug("sent request");
		producer.shutdown();
	}

	@Override
	public void onWalkmodException(Throwable e) {
	}

	@Override
	public void onWorkerException(Exception e) {
		ObjectMapper om = new ObjectMapper();
		NSQProducer producer = new NSQProducer(address, projectId);
		WorkerResponse response = new WorkerResponse();
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		response.setException(pw.toString());
		try {
			producer.put(om.writeValueAsString(response));
		} catch (Exception err) {
			LOG.error("Error", err);
		}
		LOG.debug("NSQ request sended");
		producer.shutdown();
	}

	public void addDir(File root, String src, String defaultDir,
			List<String> folders) {
		File srcDir = null;
		if (src == null || src.trim().equals("")) {
			srcDir = new File(root, defaultDir);
		} else {
			if (src.startsWith("./")) {
				src = src.substring(2);
			}
			srcDir = new File(root, src);
		}
		if (srcDir.exists()) {
			String path = srcDir.getPath();
			if (path.startsWith("./")) {
				path = path.substring(2);
			}
			folders.add(path);
		}
	}

	public void addMavenFolders(File dir, List<String> folders,
			String defaultSrcDir, String defaultTestDir) throws Exception {
		File pom = new File(dir, "pom.xml");
		if (pom.exists()) {
			// it is a maven project
			Model model = null;
			FileReader reader = null;
			MavenXpp3Reader mavenreader = new MavenXpp3Reader();
			reader = new FileReader(pom);
			model = mavenreader.read(reader);
			model.setPomFile(pom);
			Map<String, Object> properties = new HashMap<String, Object>();
			properties.put("basedir", dir.getPath());
			properties.put("project", model);
			properties.put("pom", model);
			List<String> modules = model.getModules();
			String srcPath = null;
			String src = model.getBuild().getSourceDirectory();
			if (src != null) {
				Template tmpl = engine.createTemplate(src);
				StringWriter writer = new StringWriter();
				tmpl.make(properties).writeTo(writer);
				writer.flush();
				srcPath = writer.getBuffer().toString();
				writer.close();
			} else {
				srcPath = null;
			}
			String testPath = null;
			String test = model.getBuild().getTestSourceDirectory();
			if (test != null) {
				Template tmpl = engine.createTemplate(test);
				StringWriter writer = new StringWriter();
				tmpl.make(properties).writeTo(writer);
				writer.flush();
				testPath = writer.getBuffer().toString();
				writer.close();
			} else {
				testPath = null;
			}
			if (modules != null && !modules.isEmpty()) {
				if (srcPath != null) {
					defaultSrcDir = srcPath;
				}
				if (testPath != null) {
					defaultTestDir = testPath;
				}
				for (String module : modules) {
					addMavenFolders(new File(dir, module), folders,
							defaultSrcDir, defaultTestDir);
				}
			} else {
				addDir(dir, srcPath, defaultSrcDir, folders);
				addDir(dir, testPath, defaultTestDir, folders);
			}
		}
	}

	public boolean createWalkmodConfig(List<String> srcDir, String formatter,
			String[] includes, boolean force) throws Exception {
		File template = new File(cfgDir, "walkmod.xml.template");
		templateName = engine.createTemplate(template);
		File walkmodCfg = new File("walkmod.xml");
		if(formatter == null || "".equals(formatter)){
			formatter = "eclipse";
		}
		if (!walkmodCfg.exists()) {
			File formatterFile = new File(cfgDir, "formatters/" + formatter
					+ "-style.xml");
			if (!formatterFile.exists()) {
				formatterFile = new File(cfgDir, "formatters/eclipse-style.xml");
			}
			File projectFormatter = new File(formatter + "-style.xml");
			if (walkmodCfg.createNewFile() && projectFormatter.createNewFile()) {
				this.projectFormatter = projectFormatter;
				FileWriter fw = new FileWriter(walkmodCfg);
				Map<String, Object> context = new HashMap<String, Object>();
				List<ReaderConfig> readers = new LinkedList<ReaderConfig>();
				for (String directory : srcDir) {
					ReaderConfig reader = new ReaderConfig();
					reader.setPath(directory);
					if (includes != null && includes.length > 0) {
						ArrayList<String> filters = new ArrayList<String>();

						for (int i = 0; i < includes.length; i++) {
							if (includes[i].startsWith(directory)) {
								String filter = includes[i].substring(directory
										.length()+1);
								filters.add(filter);
							}
						}

						if (!filters.isEmpty()) {
							String[] aux = new String[filters.size()];
							int i = 0;
							for (String filter : filters) {
								aux[i] = filter;
								i++;
							}
							reader.setIncludes(aux);
							readers.add(reader);
						} else {
							// contains source files that shouldn't be processed
						}
					} else {
						readers.add(reader);
					}
				}
				context.put("srcDirs", readers);
				FileUtils.copyFile(formatterFile, projectFormatter);
				context.put("formatter", projectFormatter.getPath());
				context.put("force", force);
				try {
					templateName.make(context).writeTo(fw);
				} finally {
					fw.close();
				}
				// modifiedFiles.add("walkmod.xml");
				// modifiedFiles.add(formatter + "-style.xml");
			}
			return true;
		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		if (args == null || args.length < 4) {
			throw new RuntimeException("Invalid number of arguments");
		}
		ConfigureWorker worker = new ConfigureWorker(args[0], args[1], args[2],
				args[3]);
		worker.work();
	}
}
