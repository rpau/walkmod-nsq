package org.walkmod.nsq.workers;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;
import org.walkmod.OptionsBuilder;
import org.walkmod.WalkModFacade;

public class AbstractWalkmodWorker {

	private List<File> modifiedFiles;

	protected Logger LOG = Logger.getLogger(this.getClass());

	private String[] includes;

	private String[] excludes;

	private long executionTime = 0;

	public void work() {

		try {
			preWork();
		} catch (Exception e) {
			onWorkerException(e);
			return;
		}
		File file = new File("walkmod.xml");
		if (!file.exists()) {
			file = new File("walkmod.yml");
		}
		if (file.exists()) {

			WalkModFacade facade = new WalkModFacade(file, OptionsBuilder.options().offline(false).verbose(true)
					.throwException(true).printErrors(false).includes(includes).excludes(excludes), null);
			try {
				long startTime = System.currentTimeMillis();
				modifiedFiles = facade.apply();
				executionTime = System.currentTimeMillis() - startTime;
			} catch (Throwable e) {
				onWalkmodException(e);
				return;
			}
		}
		try {
			postWork();
			if (modifiedFiles != null) {
				LOG.debug("There are " + modifiedFiles.size());
			} else {
				LOG.debug("No files modified");
			}

		} catch (Exception e) {
			onWorkerException(e);
			return;
		}

	}

	public void preWork() throws Exception {

	}

	public void postWork() throws Exception {

	}

	public void onWalkmodException(Throwable e) {

	}

	public void onWorkerException(Exception e) {

	}

	public List<File> getModifiedFiles() {
		return modifiedFiles;
	}

	public void setIncludes(String[] includes) {
		this.includes = includes;
	}

	public void setExcludes(String[] excludes) {
		this.excludes = excludes;
	}

	public long getExecutionTime() {
		return executionTime;
	}

}
