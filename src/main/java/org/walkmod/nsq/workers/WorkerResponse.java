package org.walkmod.nsq.workers;

import java.util.ArrayList;

public class WorkerResponse {

	private ArrayList<String> files = new ArrayList<String>();
	
	private String exception = "";
	
	private String trace="";
	
	private boolean isInternalError = false;
	
	private long executionTime = 0;
	
	private Repository repository;

	public ArrayList<String> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<String> files) {
		this.files = files;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public String getTrace() {
		return trace;
	}

	public void setTrace(String trace) {
		this.trace = trace;
	}

	public boolean isInternalError() {
		return isInternalError;
	}

	public void setInternalError(boolean isInternalError) {
		this.isInternalError = isInternalError;
	}

	public long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}
	
	
	
}
