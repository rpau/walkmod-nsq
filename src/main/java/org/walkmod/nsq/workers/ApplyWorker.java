package org.walkmod.nsq.workers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.walkmod.nsq.NSQProducer;
import org.walkmod.nsq.exceptions.NSQException;

import org.codehaus.jackson.JsonGenerator.Feature;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class ApplyWorker extends AbstractWalkmodWorker {

   private String address;

   private JsonNode config;
   private ArrayList<String> modifiedFiles;
   private static ByteArrayOutputStream stdOut = new ByteArrayOutputStream();
   private static ByteArrayOutputStream stdErr = new ByteArrayOutputStream();

   public ApplyWorker(String address, String cfgDir, String config) throws Exception {
      this.address = address;
      this.config = null;
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(Feature.QUOTE_FIELD_NAMES, false);
      try {
         this.config = mapper.readTree(config);
      } catch (Exception e) {
         NSQException ex = new NSQException("Error reading the parameters of " + config);
         ex.setStackTrace(e.getStackTrace());
         LOG.error("Error reading the parameters of ", e);
         throw ex;
      }
   }

   @Override
   public void preWork() throws Exception {
      String[] includes = null;
      if (config.has("includes")) {
         JsonNode includesNode = config.get("includes");
         includes = new String[includesNode.size()];
         Iterator<JsonNode> it = includesNode.getElements();
         int i = 0;
         while (it.hasNext()) {
            File aux = new File(it.next().getTextValue());
            includes[i] = aux.getAbsolutePath();
            // LOG.info("including: "+includes[i]);
            i++;
         }

         setIncludes(includes);
      } else {
         LOG.info("Walkmod is applied into all the codebase");
      }

   }

   @Override
   public void postWork() throws Exception {
      modifiedFiles = new ArrayList<String>();
      List<File> files = super.getModifiedFiles();
      if (files != null) {
         for (File file : files) {
            modifiedFiles.add(file.getPath());
         }
      }

      ObjectMapper om = new ObjectMapper();

      NSQProducer producer = new NSQProducer(address, "forks");
      WorkerResponse response = new WorkerResponse();
      response.setFiles(modifiedFiles);
      response.setExecutionTime(getExecutionTime());
      Repository repo = om.readValue(config.get("repository"), Repository.class);
      response.setRepository(repo);
      producer.put(om.writeValueAsString(response));

      producer.shutdown();
   }

   @Override
   public void onWalkmodException(Throwable e) {

      ObjectMapper om = new ObjectMapper();
      NSQProducer producer = new NSQProducer(address, "forks");
      WorkerResponse response = new WorkerResponse();
      Repository repo;
      try {
         repo = om.readValue(config.get("repository"), Repository.class);
         response.setRepository(repo);
         StringWriter sw = new StringWriter();
         PrintWriter pw = new PrintWriter(sw);
         e.printStackTrace(pw);
         response.setException(sw.toString());

         stdOut.flush();
         stdErr.flush();

         sw.close();
         pw.close();

         String trace = stdOut.toString();
         String error = stdErr.toString();
         if (error != null && !"".equals(error)) {
            trace = trace + "\nEstandard Error:\n" + error;
         }
         response.setTrace(trace);
         String content = om.writeValueAsString(response);

         producer.put(content);

      } catch (Exception e1) {
         LOG.error("Error sending the user error");
      }

      producer.shutdown();

   }

   @Override
   public void onWorkerException(Exception e) {
      ObjectMapper om = new ObjectMapper();
      NSQProducer producer = new NSQProducer(address, "forks");
      WorkerResponse response = new WorkerResponse();
      Repository repo;
      try {
         repo = om.readValue(config.get("repository"), Repository.class);
         response.setRepository(repo);
         response.setInternalError(true);
         StringWriter sw = new StringWriter();
         PrintWriter pw = new PrintWriter(sw);
         e.printStackTrace(pw);
         response.setException(sw.toString());

         sw.close();
         pw.close();
         String content = om.writeValueAsString(response);

         producer.put(content);

      } catch (Exception e1) {
         LOG.error("Error sending the internal error");
      }

      producer.shutdown();
   }

   public static void main(String[] args) throws Exception {
      // Create a stream to hold the output

      if (args == null || args.length < 2) {
         throw new RuntimeException("Invalid number of arguments");
      }
      PrintStream psStd = new PrintStream(stdOut);
      PrintStream oldStd = System.out;

      PrintStream psErr = new PrintStream(stdErr);
      PrintStream oldErr = System.err;

      System.setOut(psStd);
      System.setErr(psErr);

      try {
         ApplyWorker worker = new ApplyWorker(args[0], args[1], args[2]);
         worker.work();
      } finally {
         oldStd.print(stdOut.toString());
         stdOut.close();
         psStd.close();
         System.setOut(oldStd);

         oldErr.println(stdErr.toString());
         stdErr.close();
         psErr.close();
         System.setErr(oldErr);
      }

   }
}
