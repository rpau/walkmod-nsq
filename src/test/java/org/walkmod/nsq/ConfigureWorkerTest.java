package org.walkmod.nsq;

import groovy.text.GStringTemplateEngine;
import groovy.text.Template;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.walkmod.conf.entities.ReaderConfig;
import org.walkmod.nsq.workers.ConfigureWorker;

public class ConfigureWorkerTest
{
    @Test
    public void testCfgCreation() throws Exception
    {
        ConfigureWorker worker = new ConfigureWorker( "", "", "src/test/resources", "{\"formatter\": \"eclipse\"}" );
        List<String> srcDirs = new LinkedList<String>();
        srcDirs.add( "src" );
        worker.createWalkmodConfig( srcDirs, "google", null, true );
        File walkmod = new File( "walkmod.xml" );
        Assert.assertTrue( walkmod.exists() );
        walkmod.delete();
        File formatter = new File( "google-style.xml" );
        Assert.assertTrue( formatter.exists() );
        formatter.delete();
    }

    @Test
    public void testListMavenSrcDirs() throws Exception
    {
        ConfigureWorker worker = new ConfigureWorker( "", "", "src/test/resources", "{\"formatter\": \"eclipse\"}" );
        List<String> folders = new LinkedList<String>();
        worker.addMavenFolders( new File( "." ), folders, "src/main/java", "src/test/java" );
        Assert.assertEquals( 2, folders.size() );
    }

    @Test
    public void testMavenModules() throws Exception
    {
        ConfigureWorker worker = new ConfigureWorker( "", "", "src/test/resources", "{\"formatter\": \"eclipse\"}" );
        List<String> folders = new LinkedList<String>();
        worker.addMavenFolders( new File( "./src/test/resources/poms4test" ), folders, "src/main/java", "src/test/java" );
        Assert.assertEquals( 8, folders.size() );
    }

    @Test
    public void testTemplate() throws Exception
    {
        GStringTemplateEngine engine = new GStringTemplateEngine();
        File template = new File( "src/test/resources", "walkmod.xml.template" );
        Template templateName = engine.createTemplate( template );
        StringWriter sw = new StringWriter();
        Map<String,Object> context = new HashMap<String,Object>();
        List<ReaderConfig> readers = new LinkedList<ReaderConfig>();
        ReaderConfig rc = new ReaderConfig();
        rc.setPath( "src/test/java" );
        rc.setIncludes( new String[] { "org/walkmod/nsq/ConfigureWorkerTest.java" } );
        readers.add( rc );
        context.put( "srcDirs", readers );
        context.put( "formatter", "src/test/resources/formatters/android-style.xml" );
        context.put( "force", "true" );
        try
        {
            templateName.make( context ).writeTo( sw );
            StringBuffer sb = sw.getBuffer();
            String result = sb.toString();
            Assert.assertTrue( result.contains( "<reader path=\"src/test/java\">" ) );
            Assert.assertTrue( result.contains( "<include wildcard=\"org/walkmod/nsq/ConfigureWorkerTest.java\" />" ) );
        }
        finally
        {
            sw.close();
        }
    }
}
